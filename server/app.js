const Koa        = require('koa');
const Router     = require('koa-router');
const serve      = require('koa-static-folder');
const bodyParser = require('koa-bodyparser');
const render     = require('koa-ejs');
const path       = require('path');
const config     = require('config');
const db         = require('../mongo');

const server = new Koa();
const router = new Router();

// Force Koa to use string qs mode
require('koa-qs')(server, 'strict');

// Setup Koa-EJS Rendering
render(server, {
  root:    path.join(__dirname, "..", 'views'),
  layout:  'template',
  viewExt: 'html',
  cache:   false
});

// Setup static folder service
server.use(serve('./assets'));

// Catch all error handling
server.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    const resError = {
      code: 500,
      message: e.message,
      errors: e.errors
    };
    if (e instanceof Error) {
      Object.assign(resError, {stack: e.stack});
    }
    Object.assign(ctx, {body: resError, status: e.status || 500});
  }
});

// Apply Body-Parser middleware first
server.use(bodyParser());

// Set up Routes for the application
require('./core-routes.js')(router, db);

// Consume the routes and apply them to our server
server.use(router.routes());

// Connect to the db
db.connect()
  .then(() => {
    server.listen(config.port, () => {
      console.info(`Listening to http://localhost:${config.port}`);
    });
  })
  .catch((err) => {
    console.error('ERR:', err);
  });