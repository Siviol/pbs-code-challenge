const moment           = require('moment');
const OrderValidations = require('../lib/orderValidations');
const config           = require('config');

module.exports = function(router, db) {

  router.get('/',  async (ctx) => {
    let items = (ctx.query.items || ctx.query.item) || config.menu_items;
    let before = parseInt((ctx.query.before || [])[0]) || Date.now() + 3660000;
    let after = parseInt((ctx.query.after || [])[0]) || 0;
    await ctx.render('cooktimes', {
      moment: moment,
      orders: await db.Order.findWithCookTimes({
        items:       {$in: items },
        pickup_time: {$gte: after, $lte: before},
        fulfilled:   null
      }, {"pickup_time": 1})
    });
  });

  router.get('/orders', async (ctx, next) => {
    //todo create a strict mode
    // strict = 0: orders can contain either
    // strict = 1: orders must contain both
    // strict = 2: orders must be only both
    let items  = (ctx.query.items || ctx.query.item) || config.menu_items;
    let done   = (ctx.query.fulfilled) || null;
    let before = parseInt((ctx.query.before || [])[0]) || Date.now() + 3660000;
    let after  = parseInt((ctx.query.after  || [])[0]) || 0;
    await ctx.render('orders', {
      moment: moment,
      orders: await db.Order.find({
        items:       { $in: items },
        pickup_time: { $gte: after, $lte: before },
        fulfilled:   done
      }, {"pickup_time": 1})
    });
  });

  router.get('/orders/fulfilled', async (ctx, next) => {
    await ctx.render('fulfilled', {
      moment: moment,
      orders: await db.Order.find({ fulfilled: "true" })
    });
  });

  router.get('/orders/next', async (ctx, next) => {
    await ctx.render('cooktimes', {
      moment: moment,
      orders: await db.Order.findLatest()
    });
  });

  router.get('/orders/:id', async (ctx, next) => {
    const id  = ctx.params.id;
    let order = [ await db.Order.findOneById(id) ];
    await ctx.render('orders', {
      moment: moment,
      orders: order
    });
  });

  router.get('/orders/:id/schedule', async (ctx, next) => {
    const id  = ctx.params.id;
    ctx.body  = await db.Order.returnCookTimes(id);
  });

  router.post('/orders', async (ctx, next) => {
    const order         = ctx.request.body;
    const orderValidity = OrderValidations.orderValidator(order);
    if (orderValidity.status === 200) {
      ctx.body   = await db.Order.insertOne(order);
    } else {
      ctx.status = orderValidity.status;
      ctx.body   = orderValidity.text;
    }
  });

  router.put('/orders/:id', async (ctx, next) => {
    const id    = ctx.params.id;
    const order = ctx.request.body;
    ctx.body = await db.Order.findOneAndUpdate(id, order);
  });

  router.del('/orders/:id', async (ctx, next) => {
    const id = ctx.params.id;
    ctx.body = await db.Order.removeOne(id);
  });

  router.get('/error/test', async () => {
    throw Error('Error handling works!');
  });
};