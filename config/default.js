module.exports = {
  port: 7070,
  endpoint: "127.0.0.1:7070/orders",
  concurrency: 10,
  total_requests: 50,
  menu_items: ['hamburger', 'hotdog', 'fries', 'shake'],
  cook_times: {
    hamburger: 10,
    hotdog:     7,
    fries:      3,
    shake:      2
  },
  db: {
    docker_url:  'mongodb://db:27017/machdonalds',
    machine_url: 'mongodb://localhost:27017/machdonalds'
  }
};