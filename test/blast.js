let request = require('superagent');
require('superagent-as-promised')(request);

const config         = require('config');
const ENDPOINT       = config.endpoint       || '127.0.0.1:3000';
const CONCURRENCY    = config.concurrency    || 10;
const TOTAL_REQUESTS = config.total_requests || 1000;
const MENU_ITEMS     = config.menu_items     || ['hamburger', 'hotdog', 'fries', 'shake'];

/**
 * Random int between a and b (a inclusive, b exclusive)
 */
const randInt = function(a, b) {
  return Math.floor(Math.random() * (b - a) + a);
};

const randomItem = function() {
  const items = MENU_ITEMS;
  const index = randInt(0, items.length);
  return items[index];
};

const randomEvent = function() {
  // Create between 1 and 5 random items for pickup
  const numberOfItems = randInt(1, 6);
  let items = [];
  for (let i = 0; i < numberOfItems; i++) {
    items.push(randomItem())
  }

  // Create a pickup time between 10 and 60 minutes in the future
  let minutes = randInt(10, 61);
  let pickup_time = Date.now() + minutes * 60000;

  return {
    items,
    pickup_time
  };
};

const makeRequest = function() {
  const query = JSON.stringify(randomEvent());
  return request.post(ENDPOINT).send(randomEvent())
};

const concurrentRequests = function(N) {
  let requests = [];
  for (let i = 0; i < N; i++) {
    requests.push(makeRequest());
  }
  return Promise.all(requests);
};

const doTotalRequests = function(totalRemaining, concurrency) {
  if(totalRemaining <= 0) {
    return Promise.resolve();
  }
  console.log(`${totalRemaining} remaining`);
  return concurrentRequests(concurrency)
    .then(() => {
      return doTotalRequests(totalRemaining - concurrency, concurrency); })
    .catch((err) => {
      console.log(err);
    })
};

doTotalRequests(TOTAL_REQUESTS, CONCURRENCY)
  .then(() => {
    console.log('Yay!'); })
  .catch((err) => {
  console.log(err);
});