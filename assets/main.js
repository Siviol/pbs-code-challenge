$().ready(function() {

  switch(location.pathname) {
    case "/orders":
      document.getElementById("orderLink").className += " active"; break;
    case "/orders/next":
      document.getElementById("nextLink").className += " active"; break;
    case "/orders/fulfilled":
      document.getElementById("fulfilledLink").className += " active"; break;
    default:
      break;
  }

  window.deleteOrder = function(element) {
    var orderID = element.getAttribute("data-order");
    $.ajax({
      url: '/orders/' + orderID,
      type: 'DELETE',
      success: function(result) {
        var el = document.getElementById(orderID);
        $(el).animate({
          'padding':   "0px",
          'font-size': "0px",
          'height':    "0px",
          'opacity':   "0"
        }, 500, function() {
          $(el).remove();
          if (location.pathname === "/orders/next") {
            location.reload();
          }
        });
      }
    });
  };

  window.confirmOrder = function(element) {
    var orderID = element.getAttribute("data-order");
    $.ajax({
      url: '/orders/' + orderID,
      type: 'PUT',
      data: { fulfilled: true, fulfilled_at: Date.now() },
      success: function(result) {
        var el = document.getElementById(orderID);
        $(el).animate({
          'padding':   "0px",
          'font-size': "0px",
          'height':    "0px",
          'opacity':   "0"
        }, 500, function() {
          $(el).remove();
          if (location.pathname === "/orders/next") {
            location.reload();
          }
        });
      }
    });
  };
});

