const config = require('config');

let OrderValidations = {
  orderValidator: function(order) {
    const itemValidity = OrderValidations.itemsValidator(order.items);
    const dtValidity   = OrderValidations.dtValidator(order.pickup_time);
    let   validity, validationText;
    if (itemValidity.valid && dtValidity.valid) {
      validity = 200;
      validationText = "Valid Order";
    } else {
      validity = 400;
      validationText = "Your order contained the following errors:" + "\n" +
                       itemValidity.invalidText + "\n" +
                       dtValidity.invalidText;
    }
    return { status: validity, text: validationText }
  },
  itemsValidator: function(items = []) {
    let validity = true, invalidItems = [], invalidText = "";
    if (items.length > 0) {
      items.forEach((item)=>{
        if (!config.menu_items.includes(item)) {
          validity = false;
          invalidItems.push(item)
        }
      });
      invalidText = validity ? "" : "Order contains the following invalid item(s): " + invalidItems.join(", ");
    } else {
      validity    = false;
      invalidText = "Order did not contain any items"
    }
    return { valid: validity, invalidText: invalidText }
  },
  dtValidator: function(dt = Date.now()) {
    const minutes     = new Date(dt - Date.now()).getMinutes();
    const after10     = minutes >= 10;
    const underAnHour = minutes <= 60;
    let   invalidText = "";
    invalidText       = after10 ? invalidText : "Order requires more prep time: 10 minutes minimum";
    invalidText       = underAnHour ? invalidText : "Order must be picked up within an hour of ordering";
    return { valid: after10 && underAnHour, invalidText: invalidText }
  }
};

module.exports = OrderValidations;