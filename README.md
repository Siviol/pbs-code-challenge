#### Demo Project For PBS

# -- Stack -- #
Node (7.8.0)
Koa2
MongoDB

Stack Notes: I chose the latest Node version coupled with Koa2 so that I could explore the cutting edge use of the async-await paradigm as it pertains to server calls. Overall I was really impressed by the minimalist nature of Koa2 and it's solid middleware implementations. Having previously used Express in my node projects I enjoyed the changeup in syntax and coding patterns. I definitely could see myself making the switch to Koa2 for future projects. 

I went with MongoDB for its speed in prototyping. Though I don't believe Mongo is the be all and end all, its great for getting up and running.

# -- Setup -- #
Setup should be a breeze, on a machine with Docker/Docker-Compose installed simply run:

```
#!bash

docker-compose build
```
 and then 
```
#!bash

docker-compose up
```


###   Is this your first run? ###
Now if you go to *localhost:7070* you should see a somewhat blue yet ultimately blank page.

To hydrate the database with orders; you are more than welcome to use the standard emitter, however, I have included a slightly modified emitter and associated npm tasks for convenience. Configuration for this emitter is located inside */config/default.js* To use this emitter simply run 
```
#!bash

npm run hydrate
```
This will fill the database using the standard config unless otherwise changed.
Don't forget to install all of the dependencies!

```
#!bash

npm install
```


# -- Interacting with the app -- #
Even though originally not a challenge requirement, I went ahead and created a bare-bones front-end to consume and display the queries and responses in a more meaningful way.

 * Going to `/` will take you to an order page that defaults to showing all unfulfilled orders, sorted from soonest to furthest away in regards to pickup_time, and including cooking schedule information.

 * Going to `/orders` will take you to an order page much like the above but without including the schedule information.

Both of the `^above^` routes allow for query params. Try out any combination of item or pickup queries to scope the view, however, you'd like.

 * Going to `/orders/next` will show you the order with the closest pickup_time to the current time. Including scheduling.

 * Going to `/orders/uuid/schedule` will return the JSON order response for consumption in a different app.

As an extension to the app requirements, and to round out my CRUD DAL, I included a `Delete` and `Complete` button on each order. Both buttons work, with Delete - deleting the item from the database, and Complete - updating the record to show that the order has been completed. Completed orders do not show in the main window, but instead in their own route.

 * Going to `/orders/fulfilled` will show all orders that have been marked as complete, including the time that they were marked.