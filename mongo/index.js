const MongoClient = require('mongodb').MongoClient;
const config      = require('config');
const Order       = require('./order');
let db;

class Db {
  async connect() {
    if (!db) {
      let dbURL = (process.env.inContainer === "true") ? config.db.docker_url : config.db.machine_url;
      db = await MongoClient.connect(dbURL);
      this.Order = new Order(db, 'orders');
    }
  }
}

module.exports = new Db();