const Model    = require('./model');
const ObjectId = require('mongodb').ObjectID;
const config   = require('config');

class Order extends Model {
  constructor(db, collectionName) {
    super(db, collectionName);
  }

  generateSingleOrderCookTimes(order) {
    let items = [];
    order.items.forEach((item) => {
      items.push({
        item_name:  item,
        item_time:  config.cook_times[item],
        item_start: order.pickup_time - (config.cook_times[item] * 60000)
      });
    });
    return {
      _id:         order._id,
      items:       items,
      pickup_time: order.pickup_time
    };
  }

  generateOrdersCookTimes(orders) {
    let updatedOrders = [];
    orders.forEach((order) => {
      updatedOrders.push(this.generateSingleOrderCookTimes(order));
    });
    return updatedOrders;
  }

  async findLatest() {
    const result = await this.db
      .collection(this.name)
      .find({ fulfilled: null })
      .sort({"pickup_time": 1})
      .limit(1)
      .toArray();

    if (!result) { throw new Error("Db findAll error"); }
    return [ this.generateSingleOrderCookTimes(result[0]) ];
  }

  async findWithCookTimes(findObject, sortObject) {
    const result = await this.db
      .collection(this.name)
      .find(findObject)
      .sort(sortObject)
      .toArray();

    if (!result) { throw new Error("Db findAll error"); }
    return this.generateOrdersCookTimes(result);
  }

  async returnCookTimes(id) {
    let query    = { _id: ObjectId(id) };
    const result = await this.db
      .collection(this.name)
      .findOne(query);
    if (!result) { throw new Error('Db findOneById error in cookTimes'); }
    return this.generateSingleOrderCookTimes(result);
  }
}

module.exports = Order;