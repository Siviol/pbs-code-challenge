# Set the base image to Ubuntu
FROM ubuntu

# File Author / Maintainer
MAINTAINER Ben R Harrison

# Create and Define working directory
RUN mkdir -p /src
WORKDIR /src

# Create MongoDB data/db
RUN mkdir ./data
RUN mkdir ./data/db

# Install Node.js and others
# Then install the real nodejs
# Then remove the oldjs
RUN apt-get update && \
    apt-get -y install curl nodejs npm git mongodb && \
    npm install -g n && \
    n latest && \
    apt-get -y remove nodejs npm && \
    hash -r

# Provides cached layer for node_modules
COPY package.json /tmp/package.json
RUN npm config set registry http://registry.npmjs.org/ && \
    cd /tmp && npm install && mkdir -p /src/node_modules && \
    cp -a /tmp/node_modules /src/

# Copy everything into the container
COPY . ./

# Expose port to the outside
EXPOSE 7070

# Start the server
CMD npm start